<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class LunchController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index()
    {
      $avail_ing = Self::ingredient_avail();
      $url = storage_path().'/json/recipes.json';
      $json = json_decode(file_get_contents($url), true);
      $data = array_filter($json)['recipes'];
      foreach($data as $data_rcp) {
        $result_ings = $data_rcp['ingredients'];
        $contains = count(array_intersect($avail_ing, $result_ings)) == count($result_ings);
        if($contains){
            $result_rcp[] = $data_rcp;
        }
      }
      echo json_encode(array("recipes" => $result_rcp));
    }

    public function ingredient_avail()
    {
      $url = storage_path().'/json/ingredients.json';
      $json = json_decode(file_get_contents($url), true)['ingredients'];
      $ingre = array_filter($json);
      $curr_date = date("Y-m-d");
      $ingre = collect($json)->where("use-by",">",$curr_date)->sortByDesc('use-by')->all();
      foreach($ingre as $ingr) {
        $result[] = $ingr['title'];
      }
      return $result;
    }

}
