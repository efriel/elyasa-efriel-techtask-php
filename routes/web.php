<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/lunch', 'LunchController@index')->name('lunch');
Route::get('/ingredient_avail', 'LunchController@ingredient_avail')->name('ingredient_avail');
